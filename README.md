# emacs-ci docker images

Scripts to create CI Docker images for Emacs packages.

## buildball

Download and build several versions of GNU Emacs. 

Each version is downloaded, configured and built.  It is then
installed into a local DESTDIR and packaged as a tarball for later
inclusion in a Docker image.  The prefix for each Emacs version is
`/opt/emacs/VERSION`.

As GNU Emacs does not build in the Docker containers used by most
(any?) CI services for security reasons, Emacs is built outside of
`docker build'.  For this reason it is important that the GNU/Linux
machine used to build the tarballs matches the base Docker image used
later.  I've chosen to use Debian 9.

## builddocker

Build several docker images with various version of GNU Emacs
installed.

A set of Dockerfiles are used to build one Docker image for each
version of GNU Emacs.  There are also some images built for a range of
versions.

Each Docker image also comes with

* make
* bzip2
* curl
* git

update-alternatives` is used in each image to provide an emacs`command
symlink.  One alternative per version is added for the multi-version
images, with the highest priority given to the last version.

